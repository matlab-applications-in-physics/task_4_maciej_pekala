%MATLAB R2020b
%name:count_pulses
%author:maciej_pekala
%date: 02.01.2021
%version: v1.0

function [liczba_impulsow] = count_pulses(poziom_szumu, nazwa_pliku)
    %otwarcie pliku
    plik =fopen(nazwa_pliku);

    % deklaracja wielkości części pliku
    rozmiar = 10000000;
    i=0;
    wszystkie_zliczenia =0; 
    liczba_impulsow= 0;
 
    % zbieranie danych do histogramu
    while ~feof(plik)

        fseek(plik, rozmiar*i, 'bof');
        data = fread(plik,rozmiar,'uint8');

        [zliczenia,przedzialy] = histcounts(data,'BinLimits',[0.5,200.5],'BinWidth', 1);
        wszystkie_zliczenia = wszystkie_zliczenia + zliczenia;
        i=i+1;
        
        granica_szumu=25;
        
        %podzial danych na czesci po 500 sygnalow
        for k = 1:500:length(data)-500
            max_wartosc=0;
            max_wartosc = max(max_wartosc,max(data(k:k+499)-poziom_szumu));
            
            %sprawdzenie czy w danej czesci znajduje sie impuls
            if max_wartosc > granica_szumu
                liczba_impulsow = liczba_impulsow +1

            end
        end  
    end
end

