%MATLAB R2020b
%name:peak_Anakysis
%author:maciej_pekala
%date: 02.01.2021
%version: v1.0

% otwarcie pliku
plik =fopen('Co60_1350V_x20_p7_sub.dat');

% deklaracja wielkości części pliku
rozmiar = 10000000;
i=0;
wszystkie_zliczenia =0; 


 
% zbieranie danych do histogramu
while ~feof(plik)
    
    fseek(plik, rozmiar*i, 'bof');
    data = fread(plik,rozmiar,'uint8');
    
    [zliczenia,przedzialy] = histcounts(data,'BinLimits',[0.5,200.5],'BinWidth', 1);
    wszystkie_zliczenia = wszystkie_zliczenia + zliczenia;
    i=i+1;
end

%stworzenie histogramu
histogram('BinEdges',przedzialy,'BinCounts',wszystkie_zliczenia);
xlabel('przedzial');
ylabel('ilosc_zliczen');

%zapisanie histogramu do pliku
saveas(gcf,'Co60_1350V_x20_p7_sub_histogram.pdf');

%wywolanie funkcji
poziom_szumu= bias(przedzialy, wszystkie_zliczenia);
liczba_impulsow= count_pulses(poziom_szumu, 'Co60_1350V_x20_p7_sub.dat');

%zapisywanie do pliku
wyniki_1= fopen('Co60_1350V_x20_p7_sub_peak_Analysis_results.dat', 'w');
fprintf(wyniki_1, 'Co60_1350V_x20_p7_sub\n');
fprintf(wyniki_1, "poziom szumu = %.0f\n", poziom_szumu);
fprintf(wyniki_1, "liczba impulsow = %.0f\n", liczba_impulsow);

fclose('all');