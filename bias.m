%MATLAB R2020b
%name:bias
%author:maciej_pekala
%date: 02.01.2021
%version: v1.0


%wyszukuje najczesciej wystepujaca wartosc
function [poziomy_szumu] = bias(przedzialy, wszystkie_zliczenia) 
    max_zliczenie=0;

    for wartosc= 1:200
         if max_zliczenie < wszystkie_zliczenia(wartosc)
             poziomy_szumu = wartosc
             max_zliczenie= wszystkie_zliczenia(wartosc)
             
         end
    end


end